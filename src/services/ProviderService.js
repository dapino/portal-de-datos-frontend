import fetch from 'isomorphic-fetch';

import { URL_CONSULTAS, URL_BUSQUEDAS, KEY_TYPE} from '../config/const';
//import { URL_CONSULTAS, URL_BUSQUEDAS, URL_API, KEY_TYPE} from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getProviderList(pageOfItems, search, orderBy) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }
  return request(xhr, 'GET', `${URL_BUSQUEDAS}/api/agg?agg=organizationId&keyType=${KEY_TYPE}&page=${pageOfItems}${search && '&' + search}`)
//  return request(xhr, 'GET', `${URL_API}/api/provider/list?keyType=${KEY_TYPE}&page=${pageOfItems}${search ? '&' + search : ''}${orderBy ? '&ord=' + orderBy : ''}`)
}

export function getProviderSearchList(limit, search) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }
  return request(xhr, 'GET', `${URL_BUSQUEDAS}/api/agg2?agg=organizationId&keyType=${KEY_TYPE}&limit=${limit}${search && '&' + search}`)
//  return request(xhr, 'GET', `${URL_API}/api/provider/searchresource?keyType=${KEY_TYPE}&page=${pageOfItems}${search && '&' + search}${orderBy ? '&ord=' + orderBy : ''}`)
}

export function getProvider(id) {
  return fetch(`${URL_CONSULTAS}/api/provider/${id}`, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}
