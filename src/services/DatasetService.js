import fetch from 'isomorphic-fetch';

import { URL_CONSULTAS, URL_BUSQUEDAS, KEY_TYPE } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getDatasetGrid(page, search, orderBy) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }
  return request(xhr, 'GET', `${URL_BUSQUEDAS}/api/agg?agg=gbifId&keyType=${KEY_TYPE}&page=${page}${search && '&' + search}`)
}

export function getDatasetList(limit, search) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }
  return request(xhr, 'GET', `${URL_BUSQUEDAS}/api/agg2?agg=gbifId&keyType=${KEY_TYPE}&limit=${limit}${search && '&' + search}`)
}

export function getDataset(id) {
  return fetch(`${URL_CONSULTAS}/api/dataset/${id}`, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}


