import { URL_BUSQUEDAS, KEY_TYPE_COUNTS } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getStats(id, param) {
  let url = URL_BUSQUEDAS + '/api/stats'
  if (typeof id !== 'undefined' && typeof param !== 'undefined'){
    url += `?keyType=${KEY_TYPE_COUNTS}&${id}=${param}`
  }else{
    url += `?keyType=${KEY_TYPE_COUNTS}`
  }

  return request(xhr, 'GET', url)
}
