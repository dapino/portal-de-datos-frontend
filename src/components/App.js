import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, HashRouter } from 'react-router-dom';

import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import Home from './pages/Home';
import Search from './pages/Search';
import Occurrence from './pages/details/Occurrence';
import Providers from './pages/Providers';
import Provider from './pages/details/Provider';
import Datasets from './pages/Datasets';
import Dataset from './pages/details/Dataset';
import GeograficExplorer from './pages/GeograficExplorer';
import Static from './pages/Static';
import NotFound from './pages/NotFound';

UIkit.use(Icons);

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/search/:tab?" component={Search} />
          <Route path="/occurrence/:id" component={Occurrence} />
          <Route path="/providers" component={Providers} />
          <Route path="/provider/:id" component={Provider} />
          <Route path="/datasets" component={Datasets} />
          <Route path="/dataset/:id" component={Dataset} />
          <Route path="/geografic/explorer" component={GeograficExplorer} />
          <Route path="/static/:id" component={Static} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
