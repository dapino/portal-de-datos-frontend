import React from 'react';

import NotFoundBoo from '../atoms/NotFoundBoo';
import GenericPage from '../templates/GenericPage';
import Header from '../organisms/Header';
import Footer from '../organisms/Footer';

const NotFound = () => {
  return (
    <GenericPage titlep="Portal de Datos" header={<Header />} footer={<Footer />}>
      <NotFoundBoo />
    </GenericPage>
  )
}

export default NotFound;
