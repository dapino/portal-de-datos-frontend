import React, { Component } from 'react';
import Sliderc, { createSliderWithTooltip } from 'rc-slider';

const SliderComponent = createSliderWithTooltip(Sliderc);

class Slider extends Component {

  constructor() {
    super();
    this.state = {
      value: 0,
    }
  }

  onSliderChange = (value) => {
    this.props.values(value)    
    this.setState({
      value,
    });
  }

  render() {
    return (
      <SliderComponent
        value={this.state.value}
        min={0} max={9999}
        onChange={this.onSliderChange}
        {...this.props}
      />
    )
  }
}

export default Slider;
