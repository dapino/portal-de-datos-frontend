import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import map from 'lodash/map'
import { Link } from 'react-router-dom'

import * as StatiticsService from '../../services/StatiticsService';
//TODO: Convetir a componente de visualización 
class GeneralStatistic extends Component {

  static defaultProps = {
    id: undefined,
    param: undefined
  }

  constructor() {
    super();
    this.state = {
      stats: null,
    }
  }

  componentDidMount() {
    StatiticsService.getStats(this.props.id, this.props.param).then(data => {
      let d = []
     // console.log(data)
      if (this.props.id===undefined){
        d[0] = data[0]
        d[1] = data[1]
      }else if (this.props.id==="gbifId"){
        d[0] = data[0]
        d[1] = data[3]
      }else if (this.props.id==="organizationId"){
        d[0] = data[0]
        d[1] = data[1]
      }
      this.setState({ stats: d })
    }).catch(err => {
      console.error(err)
    })
  }

  render() {
    const { stats } = this.state;
    const { providerId } = this.props;
    return (
      stats && <div className="uk-card uk-card-default uk-padding-small uk-card-body" >
        <div className="uk-grid-divider uk-child-width-expand@s uk-text-center uk-grid-small" data-uk-grid>
          {
            stats && map(stats, (v, k) => (
              <div key={k} className="uk-grid-collapse uk-child-width-1" data-uk-grid>
                <div className="uk-text-small">{v.name}</div>
                  {
                    this.props.providerId &&
                      <Link
                        to={`/search?organizationId=${providerId}`}
                        className="uk-h2">
                        <NumberFormat value={v.value} displayType="text" thousandSeparator="." decimalSeparator="," />
                      </Link>
                  }
              </div>
            ))
          }
        </div>
      </div>
    )
  }
}

export default GeneralStatistic;
