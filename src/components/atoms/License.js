import React, { Component } from 'react';
import { filter } from 'lodash';

class License extends Component {

  constructor(props) {
    super(props)


    this.licenses = [
      { id: 'CC 0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-0.png', to: 'http://creativecommons.org/publicdomain/zero/1.0/legalcode' },
      { id: 'CC BY-4.0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by.png', to: 'https://creativecommons.org/licenses/by/4.0' },
      { id: 'CC BY-NC 4.0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/svg/licencia-by-nc.svg', to: 'https://creativecommons.org/licenses/by-nc/4.0' },
      { id: 'CC_BY_NC_4_0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/svg/licencia-by-nc.svg', to: 'https://creativecommons.org/licenses/by-nc/4.0' },
      { id: 'http://creativecommons.org/publicdomain/zero/1.0/legalcode', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-0.png', to: 'http://creativecommons.org/publicdomain/zero/1.0/legalcode' },
      { id: 'http://creativecommons.org/licenses/by/4.0/legalcode', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by.png', to: 'https://creativecommons.org/licenses/by/4.0' },
      { id: 'http://creativecommons.org/licenses/by-sa/4.0/legalcode', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-sa.png', to: 'https://creativecommons.org/licenses/by-sa/4.0' },
      { id: 'http://creativecommons.org/licenses/by-nc/4.0/legalcode', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-nc.png', to: 'https://creativecommons.org/licenses/by-nc/4.0' },
      { id: 'http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-nc-sa.png', to: 'https://creativecommons.org/licenses/by-nc-sa/4.0' },
      { id: 'http://creativecommons.org/publicdomain/zero/1.0/', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-0.png', to: 'http://creativecommons.org/publicdomain/zero/1.0/legalcode' },
      { id: 'http://creativecommons.org/licenses/by/4.0/', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by.png', to: 'https://creativecommons.org/licenses/by/4.0' },
      { id: 'http://creativecommons.org/licenses/by-sa/4.0/', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-sa.png', to: 'https://creativecommons.org/licenses/by-sa/4.0' },
      { id: 'http://creativecommons.org/licenses/by-nc/4.0/', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-nc.png', to: 'https://creativecommons.org/licenses/by-nc/4.0' },
      { id: 'http://creativecommons.org/licenses/by-nc-sa/4.0/', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-nc-sa.png', to: 'https://creativecommons.org/licenses/by-nc-sa/4.0' },
      { id: 'http://creativecommons.org/publicdomain/zero/1.0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-0.png', to: 'http://creativecommons.org/publicdomain/zero/1.0/legalcode' },
      { id: 'http://creativecommons.org/licenses/by/4.0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by.png', to: 'https://creativecommons.org/licenses/by/4.0' },
      { id: 'http://creativecommons.org/licenses/by-sa/4.0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-sa.png', to: 'https://creativecommons.org/licenses/by-sa/4.0' },
      { id: 'http://creativecommons.org/licenses/by-nc/4.0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-nc.png', to: 'https://creativecommons.org/licenses/by-nc/4.0' },
      { id: 'http://creativecommons.org/licenses/by-nc-sa/4.0', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-nc-sa.png', to: 'https://creativecommons.org/licenses/by-nc-sa/4.0' },
    ]/*
    this.licenses = [
      { id: 'by', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by.png', to: 'https://creativecommons.org/licenses/by/4.0' },
      { id: 'by-sa', img: 'https://statics.sibcolombia.net/sib-resources/images/licencias/png/licencia-by-sa.png', to: 'https://creativecommons.org/licenses/by-sa/4.0' },
      { id: 'by-nd', img: '/images/licenses/by-nd.png', to: 'https://creativecommons.org/licenses/by-nd/4.0' },
      { id: 'by-nc', img: '/images/licenses/by-nc/.png', to: 'https://creativecommons.org/licenses/by-nc/4.0' },
      { id: 'by-nc-sa', img: '/images/licenses/by-nc-sa.png', to: 'https://creativecommons.org/licenses/by-nc-sa/4.0' },
      { id: 'by-nc-nd', img: '/images/licenses/by-nc-nd.png', to: 'https://creativecommons.org/licenses/by-nc-nd/4.0' },
    ]*/
  }

  render() {
    const license = filter(this.licenses, { id: this.props.id })
    return (
      <div>
        {license[0] && 
          <a href={`${license[0].to}`} target="_blank" rel="noopener noreferrer">
            <img src={license[0].img} alt={license[0].id} />
          </a>
        }
        {!license[0] && 
          <a href={`${this.props.id}`} target="_blank" rel="noopener noreferrer">
            {this.props.id}
          </a>
        }
      </div>
    )
  }
}

export default License;
