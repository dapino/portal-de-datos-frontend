import React, { Component } from 'react';

class Sample extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.materialSampleID && data.materialSampleID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID de muestra del ejemplar</span>
            </div>
            <div>
              <span className="uk-text-break">{data.materialSampleID}</span>
            </div>
          </li>
        }
      </ul>
    );
  }
}

export default Sample;
