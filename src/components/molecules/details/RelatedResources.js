import React, { Component } from 'react';

class RelatedResources extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.resourceRelationshipID && data.resourceRelationshipID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID de relación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.resourceRelationshipID}</span>
            </div>
          </li>
        }
        {
          (data.resourceID && data.resourceID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del recurso</span>
            </div>
            <div>
              <span className="uk-text-break">{data.resourceID}</span>
            </div>
          </li>
        }
        {
          (data.relatedResourceID && data.relatedResourceID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del recurso relacionado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.relatedResourceID}</span>
            </div>
          </li>
        }
        {
          (data.relationshipOfResource && data.relationshipOfResource !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Relación de recursos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.relationshipOfResource}</span>
            </div>
          </li>
        }
        {
          (data.relationshipAccordingTo && data.relationshipAccordingTo !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Relación de acuerdo a</span>
            </div>
            <div>
              <span className="uk-text-break">{data.relationshipAccordingTo}</span>
            </div>
          </li>
        }
        {
          (data.relationshipEstablishedDate && data.relationshipEstablishedDate !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fecha de relación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.relationshipEstablishedDate}</span>
            </div>
          </li>
        }
        {
          (data.relationshipRemarks && data.relationshipRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios</span>
            </div>
            <div>
              <span className="uk-text-break">{data.relationshipRemarks}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default RelatedResources;
