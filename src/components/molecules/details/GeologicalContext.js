import React, { Component } from 'react';

class GeologicalContext extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.geologicalContextID && data.geologicalContextID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del contexto geológico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.geologicalContextID}</span>
            </div>
          </li>
        }
        {
          (data.earliestEonOrLowestEonothem && data.earliestEonOrLowestEonothem !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Eón temprano o eonotema inferior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.earliestEonOrLowestEonothem}</span>
            </div>
          </li>
        }
        {
          (data.latestEonOrHighestEonothem && data.latestEonOrHighestEonothem !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Eón tardío o eonotema superior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.latestEonOrHighestEonothem}</span>
            </div>
          </li>
        }
        {
          (data.earliestEraOrLowestErathem && data.earliestEraOrLowestErathem !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Era temprana o eratema inferior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.earliestEraOrLowestErathem}</span>
            </div>
          </li>
        }
        {
          (data.latestEraOrHighestErathem && data.latestEraOrHighestErathem !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Era tardía o eratema superior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.latestEraOrHighestErathem}</span>
            </div>
          </li>
        }
        {
          (data.earliestPeriodOrLowestSystem && data.earliestPeriodOrLowestSystem !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Periodo temprano o sistema inferior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.earliestPeriodOrLowestSystem}</span>
            </div>
          </li>
        }
        {
          (data.latestPeriodOrHighestSystem && data.latestPeriodOrHighestSystem !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Periodo tardío o sistema superior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.latestPeriodOrHighestSystem}</span>
            </div>
          </li>
        }
        {
          (data.earliestEpochOrLowestSeries && data.earliestEpochOrLowestSeries !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Época temprana o serie inferior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.earliestEpochOrLowestSeries}</span>
            </div>
          </li>
        }
        {
          (data.latestEpochOrHighestSeries && data.latestEpochOrHighestSeries !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Época tardía o serie superior  </span>
            </div>
            <div>
              <span className="uk-text-break">{data.latestEpochOrHighestSeries}</span>
            </div>
          </li>
        }
        {
          (data.earliestAgeOrLowestStage && data.earliestAgeOrLowestStage !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Edad temprana o piso inferior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.earliestAgeOrLowestStage}</span>
            </div>
          </li>
        }
        {
          (data.latestAgeOrHighestStage && data.latestAgeOrHighestStage !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Edad tardía o piso superior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.latestAgeOrHighestStage}</span>
            </div>
          </li>
        }
        {
          (data.lowestBiostratigraphicZone && data.lowestBiostratigraphicZone !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Zona bioestratigráfica inferior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.lowestBiostratigraphicZone}</span>
            </div>
          </li>
        }
        {
          (data.highestBiostratigraphicZone && data.highestBiostratigraphicZone !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Zona bioestratigráfica superior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.highestBiostratigraphicZone}</span>
            </div>
          </li>
        }
        {
          (data.lithostratigraphicTerms && data.lithostratigraphicTerms !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Términos litoestratigráficos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.lithostratigraphicTerms}</span>
            </div>
          </li>
        }
        {
          (data.group && data.group !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Grupo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.group}</span>
            </div>
          </li>
        }
        {
          (data.formation && data.formation !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Formación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.formation}</span>
            </div>
          </li>
        }
        {
          (data.member && data.member !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Miembro</span>
            </div>
            <div>
              <span className="uk-text-break">{data.member}</span>
            </div>
          </li>
        }
        {
          (data.bed && data.bed !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Capa</span>
            </div>
            <div>
              <span className="uk-text-break">{data.bed}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default GeologicalContext;
