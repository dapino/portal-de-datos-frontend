import React, { Component } from 'react';

import License from '../../atoms/License';

class Multimedia extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.type && data.type !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Tipo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.type}</span>
            </div>
          </li>
        }
        {
          (data.format && data.format !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Formato</span>
            </div>
            <div>
              <span className="uk-text-break">{data.format}</span>
            </div>
          </li>
        }
        {
          (data.identifier && data.identifier !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Identificador</span>
            </div>
            <div>
              <span className="uk-text-break">{data.identifier}</span>
            </div>
          </li>
        }
        {
          (data.references && data.references !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Referencias</span>
            </div>
            <div>
              <span className="uk-text-break">{data.references}</span>
            </div>
          </li>
        }
        {
          (data.title && data.title !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Título</span>
            </div>
            <div>
              <span className="uk-text-break">{data.title}</span>
            </div>
          </li>
        }
        {
          (data.description && data.description !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Descripción</span>
            </div>
            <div>
              <span className="uk-text-break">{data.description}</span>
            </div>
          </li>
        }
        {
          (data.created && data.created !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fecha de creación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.created}</span>
            </div>
          </li>
        }
        {
          (data.creator && data.creator !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Autor</span>
            </div>
            <div>
              <span className="uk-text-break">{data.creator}</span>
            </div>
          </li>
        }
        {
          (data.contributor && data.contributor !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Parte asociada</span>
            </div>
            <div>
              <span className="uk-text-break">{data.contributor}</span>
            </div>
          </li>
        }
        {
          (data.publisher && data.publisher !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Editor</span>
            </div>
            <div>
              <span className="uk-text-break">{data.publisher}</span>
            </div>
          </li>
        }
        {
          (data.audience && data.audience !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Audiencia</span>
            </div>
            <div>
              <span className="uk-text-break">{data.audience}</span>
            </div>
          </li>
        }
        {
          (data.source && data.source !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fuente</span>
            </div>
            <div>
              <span className="uk-text-break">{data.source}</span>
            </div>
          </li>
        }
        {
          (!data.license && data.license !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Licencia</span>
            </div>
            <div>
              <span className="uk-text-break"><License id={data.license} /></span>
            </div>
          </li>
        }
        {
          (data.rightsHolder && data.rightsHolder !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Titular de los derechos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.rightsHolder}</span>
            </div>
          </li>
        }
        {
          (data.datasetID && data.datasetID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del conjunto de datos</span>
            </div>
            <div>
              <span className="uk-text-break">{data.datasetID}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default Multimedia;
