import React, { Component } from 'react';

class MeasuresFacts extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.measurementID && data.measurementID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Identificador</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementID}</span>
            </div>
          </li>
        }
        {
          (data.measurementType && data.measurementType !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Tipo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementType}</span>
            </div>
          </li>
        }
        {
          (data.measurementValue && data.measurementValue !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Valor</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementValue}</span>
            </div>
          </li>
        }
        {
          (data.measurementAccuracy && data.measurementAccuracy !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Precisión</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementAccuracy}</span>
            </div>
          </li>
        }
        {
          (data.measurementUnit && data.measurementUnit !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Unidad de medida</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementUnit}</span>
            </div>
          </li>
        }
        {
          (data.measurementDeterminedBy && data.measurementDeterminedBy !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Determinado por</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementDeterminedBy}</span>
            </div>
          </li>
        }
        {
          (data.measurementDeterminedDate && data.measurementDeterminedDate !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fecha</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementDeterminedDate}</span>
            </div>
          </li>
        }
        {
          (data.measurementMethod && data.measurementMethod !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Método</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementMethod}</span>
            </div>
          </li>
        }
        {
          (data.measurementRemarks && data.measurementRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios</span>
            </div>
            <div>
              <span className="uk-text-break">{data.measurementRemarks}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default MeasuresFacts;
