import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

class DatasetsRow extends Component {

  render() {
    const { data } = this.props;
    let x = data.hits.hits.hits[0]._source
    return (
      <tr>
        <td className="uk-text-truncate"><Link to={`/dataset/${data.key}`} className="uk-link-reset">{x.titleResource}</Link></td>
        <td><Link to={`/dataset/${data.key}`} className="uk-link-reset">{<NumberFormat value={data.doc_count} displayType="text" thousandSeparator />}</Link></td>
        <td className="uk-text-truncate"><Link to={`/dataset/${data.key}`} className="uk-link-reset">{x.organizationName}</Link></td>
      </tr>
    );
  }
}

export default DatasetsRow;
