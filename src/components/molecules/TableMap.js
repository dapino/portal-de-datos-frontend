import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { map } from 'lodash'

class TableMap extends Component {
  ubicacion(r) {
    if(r === undefined)
      return ""
       
    var t = ""
    if (r[0]) {
      const n = parseFloat(r[0], 10)
      if (n > 0)
        t += n.toFixed(2) + " N"
      else
        t += (-n).toFixed(2) + " S"
    }
    if (r[1]) {
      const n = parseFloat(r[1], 10)
      t += ", "
      if (n > 0)
        t += n.toFixed(2) + " E"
      else
        t += (-n).toFixed(2) + " W"
    }
    return t
  }
  
  render() {
    let d = this.props.data

    return (
      <div className="uk-overflow-auto uk-margin uk-height-max-large">
        <table className="uk-table uk-table-striped uk-table-hover uk-table-justify uk-table-middle">
          <thead>
            <tr>
              <th>Nombre cientifico</th>
              <th>OccurrenceID</th>
              <th>Base del registro</th>
              <th>Ubicación</th>
              <th>Recurso</th>
              <th>Publicador</th>
            </tr>
          </thead>
          <tbody>
            {d && map(d, (v, k)=>(
              <tr key={k}>
                <td><Link className="uk-link-reset scientificName" to={"/occurrence/"+v.data.id}>{v.data.scientificName}</Link></td>
                <td><Link className="uk-link-reset" to={"/occurrence/"+v.data.id}>{v.data.id}</Link></td>
                <td><Link className="uk-link-reset" to={"/occurrence/"+v.data.id}>{v.data.basisOfRecord}</Link></td>
                <td><Link className="uk-link-reset" to={"/occurrence/"+v.data.id}>{this.ubicacion(v.position)}</Link></td>
                <td><Link className="uk-link-reset" to={"/occurrence/"+v.data.id}>{v.data.title_resource}</Link></td>
                <td><Link className="uk-link-reset" to={"/occurrence/"+v.data.id}>{v.data.organizationName}</Link></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default TableMap;
