import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { findIndex, findKey, map, differenceWith, isEqual, split, isEmpty } from 'lodash';

import Filters from '../Filters';
import * as FacetService from '../../../services/FacetService';

const xhr = new XMLHttpRequest()

class Habitat extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: null,
      modified: false,
      search: props.search
    }
    
    this.query = [];
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.setState({ search: this.props.search }, () => {
      this.fillValues('', decodeURI(this.state.search))
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.search !== this.props.search) {
        this.setState({ search: nextProps.search }, () => {
        const search = split(this.state.search, '&')
        let nSearch = []
        map(search, (v1) => {
          let insert = false
          if(findIndex(this.query, (o) => o.id === v1) < 0) 
            insert = true

          if(insert)
            nSearch.push(v1)
        })
        this.fillValues('', nSearch.join('&'))
      })
    }
  }


  fillValues(autocom, search) {
    this.setState({ values: [] }, () => {
    //this.setState({ values: [] }, () => {
      FacetService.getAuto(xhr, "habitat", autocom, search)
        .then(data => {
          this.llenarCampos(data, "habitat");
        })
        .catch(() => {
          this.setState({ values: [] })
        })
    })
  }

  llenarCampos(data, x){
      
    console.log("El resultado de la búsqueda es: ", data)
    
    //const total = 1
    const campos = data.suggest[x+"S"][0].options
    console.log(campos)
    
    let values = []
    
    map(campos, (v, k) => {
      values.push(
        {
          id: x+'=' + v.text.trim(), 
          label: v.text, 
          idQ: k, 
          labelQ: x, 
          value: v.doc_count, 
          fraction: 0
        })
    })

    this.setState({ values }, () => {!this.state.modified && this.activeFilters(this.props.activeFilters)})        
  
  }
  
  handleFilter(value) {
    const i = findIndex(this.state.values, (o) => { return o.label === value });
    const obj = this.state.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.push(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);
    this.fillValues()
    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  handleChange(e) {
    //const arg = '&habitat=' + e
    this.fillValues(e)
  }

  activeFilters(data) {
    const available = ['habitat']
    map(data, (v) => {
      const sp = split(v, '=')
      findIndex(available, (o) => {
        if (o === sp[0]) {
          if (this.state.values) {
            const i = findKey(this.state.values, (o) => { return o.id === sp[0] + '=' + decodeURI(sp[1]) })
            if (i >= 0) {
              this.handleFilter(this.state.values[i].label)
            }
          }
        }
      })
    })

    this.setState({ modified: true })
  }

  render() {
    return (
      <Filters.Base title="Hábitat" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)} empty={isEmpty(this.state.values)}>
        {this.state.values &&
          <Autocomplete 
            values={this.getValues(this.state.values)} 
            placeholder="Escribe el nombre del hábitat" 
            selectValue={(value) => this.handleFilter(value)} 
            onChange={(e) => {this.handleChange(e)}}
          />
        }
      </Filters.Base>
    );
  }
}

export default Habitat;
