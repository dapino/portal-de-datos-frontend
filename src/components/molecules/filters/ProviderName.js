import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex, split, isEmpty } from 'lodash';
import NumberFormat from 'react-number-format';

import Filters from '../Filters';
import * as FacetService from '../../../services/FacetService';

const xhr = new XMLHttpRequest()

class ProviderName extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      values: null,
      modified: false,
      search: props.search
    }

    this.query = [];
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.setState({ search: this.props.search }, () => {
      this.fillValues('', decodeURI(this.state.search))
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.search !== this.props.search) {
        this.setState({ search: nextProps.search }, () => {
        const search = split(this.state.search, '&')
        let nSearch = []
        map(search, (v1) => {
          let insert = false
          if(findIndex(this.query, (o) => o.id === v1) < 0) 
            insert = true

          if(insert)
            nSearch.push(v1)
        })
        this.fillValues('', nSearch.join('&'))
      })
    }
  }



  fillValues(autocom, search) {
    this.setState({ values: [] }, () => {
    //this.setState({ values: [] }, () => {
      FacetService.getAuto(xhr, "organizationName", autocom, search)
        .then(data => {
          this.llenarCampos(data, "organizationName");
        })
        .catch(() => {
          this.setState({ values: [] })
        })
    })
  }

  llenarCampos(data, x){

    //const total = 1
    const campos = data.suggest[x+"S"][0].options
    
    let values = []
    
    map(campos, (v, k) => {
      console.log("-> ", v._source.organizationId)
      values.push(
        {
          id: 'organizationId=' + v._source.organizationId, 
          label: v.text, 
          idQ: k, 
          labelQ: x, 
          value: v.doc_count, 
          fraction: 0
        })
    })

    this.setState({ values }, () => {!this.state.modified && this.activeFilters(this.props.activeFilters)})        
  
  }

  handleFilter(value) {
    const i = findIndex(this.state.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.state.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.push(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  handleChange(e) {
    //const arg = '&organizationName=' + e
    this.fillValues(e)
  }

  activeFilters(data) {
    const available = ['organizationName']
    map(data, (v) => {
      const sp = split(v, '=')
      findIndex(available, (o) => {
        if (o === sp[0]) {
          if (this.state.values) {
            const i = findKey(this.state.values, (o) => { return o.id === sp[0] + '=' + decodeURI(sp[1]) })
            if (i >= 0) {
              this.handleFilter(this.state.values[i].label)
            }
          }
        }
      })
    })

    this.setState({ modified: true })
  }

  render() {
    return (
      <Filters.Base title="Publicador" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)} empty={isEmpty(this.state.values)}>
        {this.state.values &&
          <Autocomplete 
            values={this.getValues(this.state.values)} 
            placeholder="Escribe el nombre del publicador" 
            selectValue={(value) => this.handleFilter(value)} 
            onChange={(e) => {this.handleChange(e)}}
          />
        }
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small" style={{
                background: "url(/images/fondo.png) no-repeat",
                backgroundSize: value.fraction + "% 100%"
              }}>
                <span>
                  <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />
                  {(value.label).substring(0, 30)}
                  {(value.label).length > 30 && "..."}
                </span>

                <span className="uk-float-right">
                  <NumberFormat value={value.value} displayType="text" thousandSeparator />
                </span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
}

export default ProviderName;
