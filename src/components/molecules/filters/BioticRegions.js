import React, { Component } from 'react';
import { map, differenceWith, isEqual, findKey } from 'lodash';

import Filters from '../Filters';
import * as FacetService from '../../../services/FacetService'

const xhr = new XMLHttpRequest()

class BioticRegions extends Component {

  constructor() {
    super();
    this.state = {
      data: null
    };
    
    this.query = [];
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters);
  }

  componentDidMount() {
    this.fillValues()
  }

  fillValues(autocom) {
    FacetService.getFacet(xhr, 'regionbiotica', autocom).then(data => {
      const fill = data.REGIONBIOTICA.counts
      let filters = []
      map(fill, (v, k) => {
        filters.push({ id: 'regionbiotica=' + k, label: v.title, idQ: k, labelQ: 'regionbiotica', value: v.count, fraction: parseInt(v.fraction * 100, 10) })
      })

      this.setState({ filters })
    })
  }

  handleFilter(value) {
    this.query.push(value)
    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.state.filters, (o) => { return o.id === decodeURIComponent(item) })
      if (i >= 0) {
        this.handleFilter(this.state.filters[i])
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Regiones bióticas" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          { 
          map((differenceWith(this.state.filters, this.query, isEqual)), (value, key) =>
            <label 
              key={key} 
              onClick={(e) => { e.preventDefault(); this.handleFilter(value) }}
              style={{ cursor: 'pointer', marginBottom: 5 }}
              className="uk-grid-collapse uk-flex-between uk-flex-middle uk-text-small" 
              data-uk-grid
              title={value.label}                
            >
              <input
                className="uk-checkbox"
                type="checkbox"
              />
              <span className="uk-width-3-4 uk-text-truncate uk-margin-small-left">{value.label}</span>
              <span className="uk-width-expand uk-text-right">{value.value}</span>
            </label>
          )
        }
        </div>
      </Filters.Base>
    )
  }
}

export default BioticRegions;
