import React, { Component } from 'react';
import { map, findKey, filter } from 'lodash';
import Filters from '../Filters';
import Range from '../../atoms/Range';
import Slider from '../../atoms/Slider';

class Elevation extends Component {

  constructor() {
    super();
    this.state = {
      label: 'Sin Filtro',
      value: [0, 9999],
      data: null,
    }

    this.query = [];
    this.filters = [{ id: '', label: 'Sin Filtro', idQ: '', labelQ: 'elevation', value: [0, 9999] }];

    this.handleSelect = this.handleSelect.bind(this);
    this.cleanFilters = this.cleanFilters.bind(this);
    this.createQuery = this.createQuery.bind(this);
    this.addRange = this.addRange.bind(this);
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  changeValues = (value, key) => {
    this.filters[key].value = value;
    this.setState({ value });
  }

  handleSelect(e, key) {
    const defaultValues = {
      'Mínima': 0,
      'Máxima': 9999,
      'Es': 0
    }

    const filter = {
      label: e.target.value,
      value: (e.target.value === 'Sin Filtro' || e.target.value === 'Entre') ? [0, 9999] : (defaultValues[e.target.value])
    }

    this.filters[key] = filter;
    this.setState({ label: filter.label, value: filter.value })
  }

  addRange(optional) {
    this.filters[this.filters.length - 1] = (optional.label === undefined ?
      { id: 'elevation=' + this.getIdQ(this.state.label, this.state.value), label: this.state.label, idQ: this.getIdQ(this.state.label, this.state.value), labelQ: 'elevation', value: this.state.value } :
      { id: 'elevation=' + this.getIdQ(optional.label, optional.parseValue), label: optional.label, idQ: this.getIdQ(optional.label, optional.parseValue), labelQ: 'elevation', value: optional.parseValue })
    this.filters.push({ id: '', label: 'Sin Filtro', idQ: '', labelQ: 'elevation', value: [0, 9999] })
    this.props.count(this.createQuery(this.filters))
    this.setState({ label: 'Sin Filtro', value: [0, 9999], data: this.filters })
  }

  getIdQ(data, args) {
    let value;
    switch (data) {
      case 'Mínima':
        value = [args, '*']
        break;
      case 'Máxima':
        value = ['*', args]
        break;

      default:
        value = args
        break;
    }
    return value

  }

  deleteRange(key) {
    this.filters.splice(key, 1)
    this.query.splice(key, 1)
    this.props.count(this.createQuery(this.filters))
    this.props.count(this.filters)
    this.setState({ data: this.filters })
  }

  cleanFilters() {
    this.filters = [{ id: '', label: 'Sin Filtro', idQ: '', labelQ: 'elevation', value: [0, 9999] }];
    this.query = []
    this.props.count(this.createQuery(this.filters))
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.filters, value);
    this.filters.splice(position, 1);
    this.query.splice(position, 1);
    this.props.count(this.createQuery(this.filters))
    this.setState({ data: this.filters })
  }

  createQuery(data) {
    let query = [];
    const filtro = filter(data, (o) => { return o.label !== 'Sin Filtro' });
    map(filtro, (value, key) => {
      query[key] = value
    })
    return query;
  }

  activeFilters(data) {
    map(data, (value, key) => {
      if (value.match(/elevation.*/)) {
        const filter = value.split('=');
        filter.shift();
        const parseFilter = filter.toString();
        let parseValue = [];
        let label;
        if (parseFilter.match(/^.*\*$/)) {
          label = 'Mínima'
          parseValue = parseInt(parseFilter.split(',')[0], 10)
        } else if (parseFilter.match(/^\*.*$/)) {
          label = 'Máxima'
          parseValue = parseInt(parseFilter.split(',')[1], 10)
        } else {
          let sv;
          if (parseFilter.split(',').length === 1) {
            label = 'Es';
            sv = parseInt(parseFilter, 10)
          } else {
            label = 'Entre'
            let m = []
            map(parseFilter.split(','), (value, key) => {
              m[key] = parseInt(value, 10)
            })
            sv = m;
          }
          parseValue = sv;
        }
        this.addRange({ label, parseValue })
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Elevación" handlerFilter={this.createQuery(this.state.data)} func={(value) => this.deleteFilter(value)}>
        <ul className="uk-list uk-list-divider" >
          {
            this.filters && map(this.filters, (value, key) =>
              <li key={key}>
                <div className="uk-grid-collapse uk-flex-between uk-margin-small" data-uk-grid="">
                  <div uk-form-custom="target: > * > span:first-child">
                    <select className="uk-select" value={value.label} onChange={(e) => { value.label === 'Sin Filtro' && this.handleSelect(e, key) }}>
                      <option value="Sin Filtro">Sin Filtro</option>
                      <option value="Es">Es</option>
                      <option value="Máxima">Máxima</option>
                      <option value="Mínima">Mínima</option>
                      <option value="Entre">Entre</option>
                    </select>
                    <button className="uk-button uk-button-text" tabIndex="-1">
                      <span>{value.label}</span>
                      <span uk-icon="icon: triangle-down; ratio: 0.8"></span>
                    </button>
                  </div>
                  {this.filters.length !== (key + 1) &&
                    <div>
                      <button type="button" data-uk-close onClick={() => this.deleteRange(key)}></button>
                    </div>
                  }
                </div>
                <div className="uk-flex-between uk-grid-collapse uk-margin-small" data-uk-grid="">
                  {(value.label === 'Es' || value.label === 'Entre' || value.label === 'Mínima') &&
                    <div className="uk-text-small">
                      {this.filters.length !== (key + 1) ?
                        (typeof value.value === 'object' ? value.value[0] : value.value)
                        :
                        (typeof this.state.value === 'object' ? this.state.value[0] : this.state.value)
                      }
                    </div>
                  }
                  {(value.label === 'Entre' || value.label === 'Máxima') &&
                    <div className="uk-text-small">
                      {this.filters.length !== (key + 1) ?
                        (typeof value.value === 'object' ? value.value[1] : value.value)
                        :
                        (typeof this.state.value === 'object' ? this.state.value[1] : this.state.value)
                      }
                    </div>
                  }
                </div>
                {value.label === 'Entre' &&
                  <Range
                    handleStyle={[
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 },
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 }
                    ]}
                    value={value.value}
                    values={(values) => this.changeValues(values, key)}
                    disabled={(value.label === 'Sin Filtro')}
                  />

                }
                {
                  value.label === 'Sin Filtro' &&
                  <Range
                    handleStyle={[
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 },
                      { backgroundColor: '#666', border: 0, width: 11, height: 11 }
                    ]}
                    value={[0, 9999]}
                    disabled
                  />
                }
                {
                  (value.label !== 'Entre' && value.label !== 'Sin Filtro') &&
                  <Slider
                    handleStyle={[{ backgroundColor: '#666', border: 0, width: 11, height: 11 }]}
                    trackStyle={[value.label === 'Máxima' ? { backgroundColor: '#666', height: 1.5 } : { backgroundColor: '#e9e9e9', height: 1.5 }]}
                    railStyle={value.label === 'Mínima' ? { backgroundColor: '#666', height: 1.5 } : { backgroundColor: '#e9e9e9', height: 1.5 }}
                    values={(values) => this.changeValues(values, key)}
                    value={value.value}
                  />
                }
              </li>
            )
          }
        </ul>
        <div className={this.filters.length > 1 ? 'uk-flex uk-flex-between' : 'uk-flex uk-flex-right'} >
          {this.filters.length > 1 &&
            <div className="uk-width-auto">
              <button className="uk-button uk-button-default uk-button-small" onClick={this.cleanFilters}>Limpiar</button>
            </div>
          }
          {this.filters[this.filters.length - 1].label !== 'Sin Filtro' &&
            <div>
              <button className="uk-button uk-button-primary uk-button-small" onClick={this.addRange}>Añadir</button>
            </div>
          }
        </div>
      </Filters.Base>
    );
  }
}

export default Elevation;
