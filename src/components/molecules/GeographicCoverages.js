import React, { Component } from 'react';
import { Rectangle } from 'react-leaflet';

import { map, set } from 'lodash';

class GeographicCoverages extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {

    const { boundingCoordinates } = this.props;
    let r = {};
    let label = ['min', 'max'];
    map(boundingCoordinates, (v, k) => {
      v = parseFloat(v);
      if (k.match(/^e.*$/) || k.match(/^w.*$/)) {
        set(r, `boundingBox.${k === 'westBoundingCoordinate' ? label[0] : label[1]}Longitude`, v)
      } else {
        set(r, `boundingBox.${k === 'southBoundingCoordinate' ? label[0] : label[1]}Latitude`, v)
      }
    })

    return <Rectangle bounds={[
      [r.boundingBox.minLatitude, r.boundingBox.minLongitude],
      [r.boundingBox.maxLatitude, r.boundingBox.maxLongitude]
    ]} />
  }
}

export default GeographicCoverages;
