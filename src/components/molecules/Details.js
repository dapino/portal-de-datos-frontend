import React, { Component } from 'react';

import Registro from './details/Registro';
import Occurrence from './details/Occurrence';
import Event from './details/Event';
import Location from './details/Location';
import Taxon from './details/Taxon';
import Identification from './details/Identification';
import Organism from './details/Organism';
import Sample from './details/Sample';
import MeasuresFacts from './details/MeasuresFacts';
import GeologicalContext from './details/GeologicalContext';
import RelatedResources from './details/RelatedResources';
import Multimedia from './details/Multimedia';
import Title from '../atoms/Title';


class Details extends Component {

  render() {
    return (
      <div>
        <Title label={this.props.title} tag="h3" underscore />
        {this.props.children} 
      </div>
    )
  }
}

Details.Registro = Registro;
Details.Occurrence = Occurrence;
Details.Event = Event;
Details.Location = Location;
Details.Taxon = Taxon;
Details.Identification = Identification;
Details.Organism = Organism;
Details.Sample = Sample;
Details.MeasuresFacts = MeasuresFacts;
Details.GeologicalContext = GeologicalContext;
Details.RelatedResources = RelatedResources;
Details.Multimedia = Multimedia;

export default Details;
