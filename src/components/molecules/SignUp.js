import React, { Component } from 'react';

import { signup/*, me*/ } from '../../auth'

class SignUp extends Component {

  constructor() {
    super()
    this.state = {
      username: '',
      password: '',
      email: '',
      name: '',
      lastname: '',
      error: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    const name = e.target.name
    const value = e.target.value

    this.setState({ [name]: value })
  }

  handleSubmit(e) {
    e.preventDefault()
    //console.log("Intentando generar ", this.state)

    signup(this.state)
      .then(data => {
        //console.log("El resultado es ", data)
        if (data.error){
          this.setState({ error: data.error })
        }else if(data.message){
          alert("El usuario no se encuentra disponible, probablemente ya se registró.")
        }else{
          this.setState({
            username: '',
            password: '',
            email: '',
            name: '',
            lastname: ''
          })
          alert("Usuario registrado correctamente, puede ingresar al sistema")
          window.location.reload()
          /*
          me()
            .then(user => {
              window.location.reload()
            })
            */
        }
      })
      .catch(err => console.error(err))
  }

  render() {
    return (
      <li>
        <form className="uk-form-stacked">
          {this.state.error !== '' &&
            <div
              className="uk-text-danger uk-text-small uk-text-bold uk-background-muted uk-border-rounded uk-flex uk-flex-middle uk-flex-center"
              style={{ padding: 10 }}
            >
              <span uk-icon="icon: warning" className="uk-margin-small-right" />
              {this.state.error}
            </div>
          }
          <div className="uk-margin-small">
            <label className="uk-form-label" htmlFor="email">Correo electrónico</label>
            <div className="uk-form-controls">
              <input className="uk-input" id="new-email" name="email" type="text" onChange={this.handleChange} />
            </div>
          </div>
          <div className="uk-margin-small">
            <label className="uk-form-label" htmlFor="new-username">Usuario</label>
            <div className="uk-form-controls">
              <input className="uk-input" id="new-username" name="username" type="text" onChange={this.handleChange} />
            </div>
          </div>
          <div className="uk-margin-small">
            <label className="uk-form-label" htmlFor="new-password">Contraseña</label>
            <div className="uk-form-controls">
              <input className="uk-input" id="new-password" name="password" type="password" onChange={this.handleChange} />
            </div>
          </div>
          <button className="uk-button uk-button-small uk-text-bold uk-button-primary uk-width-1" onClick={this.handleSubmit}>Siguiente</button>
        </form>
      </li>
    )
  }
}

export default SignUp;
