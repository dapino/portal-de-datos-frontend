import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { indexOf } from 'lodash';

import Search from '../molecules/Search';
import SignIn from '../molecules/SignIn';
import SignUp from '../molecules/SignUp';
//import Proximamente from '../molecules/Proximamente';
//import { URL_LOGO, USER } from '../../config/const';
import { URL_LOGO } from '../../config/const';
import { isAuthenticated, logout } from '../../auth'

class Header extends Component {

  constructor() {
    super();

    this.toggleMenu = this.toggleMenu.bind(this)
    this.openAdvanceSearch = this.openAdvanceSearch.bind(this)
  }

  toggleMenu() {
    this.props.menu()
  }

  openAdvanceSearch() {
    this.props.openAdvanceSearch()
  }

  preventHideMenu(e) {
    const ids = ['username', 'password', 'email', 'search', 'search-l'];
    const tags = ['INPUT', 'LABEL', 'A'];
    if (indexOf(tags, e.target.tagName) < 0 || indexOf(ids, e.target.id) >= 0) {
      e.preventDefault()
    }
    //console.log(e.target.tagName !== 'INPUT', e.target.id === 'username')
    //console.log((e.target.tagName !== 'INPUT' || e.target.tagName !== 'LABEL  ') || (e.target.id && e.target.id === 'username') && e.preventDefault())
  }

  render() {
    const me = isAuthenticated()
    let username
    if (me)
      username = me.username

/*

                  

*/
    return (
      <div id="navbar" onClick={this.preventHideMenu} className="uk-box-shadow-small uk-background-default" uk-sticky="sel-target: !nav; cls-active: uk-navbar-sticky">
        <nav data-uk-navbar="mode: hover">
          {
            this.props.withSidebar && <div className="uk-animation-slide-left uk-background-primary uk-navbar-left">
              <button
                data-uk-toggle="target: #my-id"
                className="uk-margin-small-left uk-margin-small-right uk-navbar-toggle uk-text-default"
                uk-navbar-toggle-icon=""
                onClick={this.toggleMenu}
              />
            </div>
          }
          <div className="uk-margin-small-left uk-navbar-left uk-visible@s">
            <button className="uk-navbar-toggle uk-hidden@s" uk-navbar-toggle-icon="" />
            <Link className="uk-logo" to="/">
              <img src={URL_LOGO} alt="" style={{ maxWidth: 'none' }} />
            </Link>
          </div>
          <div className="uk-margin-small-left uk-navbar-center uk-hidden@s">
            <Link className="uk-logo" to="/">
              <img src={URL_LOGO} alt="" style={{ maxWidth: 'none' }} />
            </Link>
          </div>
          {/*<Search openAdvanceSearch={this.openAdvanceSearch} search={this.props.search} path={this.props.location} />*/}
          <div className="uk-navbar-right log">
            <button className="uk-navbar-toggle uk-hidden@m" uk-search-icon="" />
            <Search.Responsive />
            <ul className="uk-navbar-nav uk-visible@s">
              <li className="uk-margin-large-right">
                <a>Explorar<span data-uk-icon="icon: triangle-down"></span></a>
                <div className="uk-width-auto" data-uk-drop="mode: hover; offset: 10">
                  <div className="uk-card uk-card-default">
                    <div className="uk-padding-small">
                      <ul className="uk-nav uk-nav-default">
                        <li><Link to="/search"><span className="uk-margin-small-right uk-icon uk-icon-image" style={{ backgroundImage: 'url(/images/registros.png)' }}></span>Registros</Link></li>
                        <li><Link to="/datasets"><span className="uk-margin-small-right uk-icon uk-icon-image" style={{ backgroundImage: 'url(/images/recursos.png)' }}></span>Recursos</Link></li>
                        <li><Link to="/providers"><span className="uk-margin-small-right uk-icon uk-icon-image" style={{ backgroundImage: 'url(/images/publicadores.png)' }}></span>Publicadores</Link></li>
                        {/*<li><Link to="/geografic/explorer"><span className="uk-margin-small-right" data-uk-icon="icon: location"></span>Explorador geografico</Link></li>*/}
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              {!isAuthenticated() &&
                <li className="uk-margin-small-right">
                  <a>Ingresar</a>
                  <div id="drop-login" data-uk-drop="mode: hover; offset: 10">
                    <div className="uk-card uk-card-default">
                      <div className="uk-padding-small">
                        <ul className="uk-child-width-expand" data-uk-tab="connect: .switcher-log">
                          <li><a>Iniciar sesión</a></li>
                          <li><a>Registro</a></li>
                        </ul>
                        <ul className="uk-switcher switcher-log">
                          <SignIn />
                          <SignUp />
                        </ul>
                      </div>
                    </div>
                  </div>
                </li>
              }
              {isAuthenticated() &&
                <li className="uk-margin-small-right">
                  <a className="" style={{ textTransform: 'none' }}>¡Bienvenido &nbsp;<span className="uk-text-bold">{username}</span>!</a>
                  <div className="uk-width-small" data-uk-drop="mode: hover; offset: 10">
                    <div className="uk-card uk-card-default">
                      <div className="uk-padding-small">
                        <ul className="uk-nav uk-nav-default">
                          <li><a onClick={(e) => { e.preventDefault(); logout() }}>Cerrar sesión</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </li>
              }
            </ul>
          </div>
        </nav>
      </div>
    );
/*
                    <div className="uk-card uk-card-default">
                      <div className="uk-padding-small">
                        <ul className="uk-child-width-expand" data-uk-tab="connect: .switcher-log">
                          <li><a>Iniciar sesión</a></li>
                          <li><a>Registro</a></li>
                        </ul>
                        <ul className="uk-switcher switcher-log">
                          <SignIn />
                          <SignUp />
                        </ul>
                      </div>
                    </div>


                    <div className="uk-card uk-card-default">
                      <div className="uk-padding-small">
                        <ul className="uk-child-width-expand" data-uk-tab="connect: .switcher-log">
                          <li><a>Iniciar sesión</a></li>
                          <li><a>Registro</a></li>
                        </ul>
                        <ul className="uk-switcher switcher-log">
                          <SignIn />
                          <SignUp />
                          <Proximamente />
                          
                        </ul>
                      </div>
                    </div>

*/    
  }
}

export default Header;
