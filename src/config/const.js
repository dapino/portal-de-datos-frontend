export const URL_LOGO = 'https://statics.sibcolombia.net/sib-resources/images/logos-canales/png/logo-datos-corte.png';
export const PATH = window.location.origin.indexOf('localhost') > -1 ? 'http://localhost:3000' : window.location.origin;
export const TOKEN = 'token-potal'
export const USER = 'portal-user'
export const URL_GBIF = 'http://api.gbif.org';

export const URL_LISTAS = 'http://listas.biodiversidad.co/';
export const URL_COLECCIONES = 'http://colecciones.biodiversidad.co/';
export const URL_PORTAL = 'http://datos.biodiversidad.co/';
export const URL_CATALOGO = 'http://catalogo.biodiversidad.co/';

export const URL_BUSQUEDAS = process.env.REACT_APP_URL_BUSQUEDAS;
export const URL_CONSULTAS = process.env.REACT_APP_URL_CONSULTAS;
export const URL_DESCARGAS = process.env.REACT_APP_URL_DESCARGAS;
export const URL_USUARIOS = process.env.REACT_APP_URL_USUARIOS;
export const URL_ESTATICOS = process.env.REACT_APP_URL_ESTATICOS;

export const KEY_TYPE = 'occurrence&keyType=specimen'
export const KEY_TYPE_COUNTS = 'occurrence&keyType=specimen&keyType=checklist'
export const CORETYPE = 'occurrence&coreType=samplingevent'

