
# Portal de datos

El proyecto del Portal de datos está implementado sobre herramientas de software de Código Abierto. El sistema operativo es GNU/Linux en cualquiera de sus distribuciones. Sobre él se instala el Motor de Base de datos MongoDB para el almacenamiento de información dentro del API para servicios del Portal de Datos. Se maneja Redis como almacén de datos para el manejo de sesiones y de usuarios. NodeJs para la ejecución del frontend y el backend. NodetomicAPI como proyecto base para el desarrollo del API interna del frontend. Git para manejo del repositorio de código fuente. Npm para el manejo de dependencias dentro del proyecto, hay muchos paquetes que no se mencionan hacen parte de las dependencias, esta información se encuentra en el archivo:

https://gitlab.com/sib-colombia/portal-de-datos-frontend/blob/develop/package.json#L14

## GNU/Linux
Linux es un sistema operativo: un conjunto de programas que le permiten interactuar con su ordenador y ejecutar otros programas.

Un sistema operativo consiste en varios programas fundamentales que necesita el ordenador para poder comunicar y recibir instrucciones de los usuarios; tales como leer y escribir datos en el disco duro, cintas, e impresoras; controlar el uso de la memoria; y ejecutar otros programas. La parte más importante de un sistema operativo es el núcleo. En un sistema GNU/Linux, Linux es el núcleo. El resto del sistema consiste en otros programas, muchos de los cuales fueron escritos por o para el proyecto GNU. Dado que el núcleo de Linux en sí mismo no forma un sistema operativo funcional, preferimos utilizar el término “GNU/Linux” para referirnos a los sistemas que la mayor parte de las personas llaman de manera informal “Linux”. 

## MongoDB
MongoDB (que proviene de «humongous») es la base de datos NoSQL líder y permite a las empresas ser más ágiles y escalables. Organizaciones de todos los tamaños están usando MongoDB para crear nuevos tipos de aplicaciones, mejorar la experiencia del cliente, acelerar el tiempo de comercialización y reducir costes.

Es una base de datos ágil que permite a los esquemas cambiar rápidamente cuando las aplicaciones evolucionan, proporcionando siempre la funcionalidad que los desarrolladores esperan de las bases de datos tradicionales, tales como índices secundarios, un lenguaje completo de búsquedas y consistencia estricta.

MongoDB ha sido creado para brindar escalabilidad, rendimiento y gran disponibilidad, escalando de una implantación de servidor único a grandes arquitecturas complejas de centros multidatos. MongoDB brinda un elevado rendimiento, tanto para lectura como para escritura, potenciando la computación en memoria (in-memory). La replicación nativa de MongoDB y la tolerancia a fallos automática ofrece fiabilidad a nivel empresarial y flexibilidad operativa. 
## Redis
Redis es un almacén de estructura de datos de valores de clave en memoria rápido y de código abierto. Redis incorpora un conjunto de estructuras de datos en memoria versátiles que le permiten crear con facilidad diversas aplicaciones personalizadas. Entre los casos de uso principales de Redis se encuentran el almacenamiento en caché, la administración de sesiones, pub/sub y las clasificaciones. Es el almacén de valores de clave más popular en la actualidad. Tiene licencia BSD, está escrito en código C optimizado y admite numerosos lenguajes de desarrollo. Redis es el acrónico de REmote DIctionary Server (servidor de diccionario remoto). 
## Nginx
NGINX es un servidor web HTTP de código abierto que incluye servicios de correo electrónico con acceso al Internet Message Protocol (IMAP) y al servidor Post Office Protocol (POP). Además, NGINX está listo para ser utilizado como un proxy inverso. En este modo, se utiliza para equilibrar la carga entre los servidores back-end, como también para ser utilizado como caché en un servidor back-end lento.

Su arquitectura, es diferente al modelo tradicional, de crear una instancia por cada request. NGINX procesa decenas de miles de conexiones simultáneas en un proceso compacto y con varios núcleos de CPU.

Además se compone de módulos que se incluyen en tiempo de compilación. Eso significa que el usuario descarga el código fuente y selecciona qué módulos quiere utilizar, haciendo más liviano que la competencia. Hay módulos para la conexión a clones de aplicaciones, balanceo de carga, servidor proxy, y otros. No hay módulo para PHP, ya que Nginx puede interpretar código PHP en sí mismo. 

## NodeJs
Node.js® es un entorno de ejecución para JavaScript construido con el motor de JavaScript V8 de Chrome. Node.js usa un modelo de operaciones E/S sin bloqueo y orientado a eventos, que lo hace liviano y eficiente. El ecosistema de paquetes de Node.js, npm, es el ecosistema mas grande de librerías de código abierto en el mundo. 

## NPM
Node Package Manager o simplemente npm es un gestor de paquetes, el cual hará más fáciles nuestras vidas al momento de trabajar con Node, ya que gracias a él podremos tener cualquier librería disponible con solo una línea de código, npm nos ayudará a administrar nuestros módulos, distribuir paquetes y agregar dependencias de una manera sencilla. 

## Nodetomic API
Es un api RESTful para Nodejs  diseñada para realizar escalabilidad horizontal, basada en Swagger, Redis, JWT, Passport, Socket.io, Express, MongoDB. Soporta múltiple cluster! 

## GIT
Git (pronunciado "guit"​) es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente. Al principio, Git se pensó como un motor de bajo nivel sobre el cual otros pudieran escribir la interfaz de usuario o front end como Cogito o StGIT. Sin embargo, Git se ha convertido desde entonces en un sistema de control de versiones con funcionalidad plena. Hay algunos proyectos de mucha relevancia que ya usan Git, en particular, el grupo de programación del núcleo Linux. 


# Instrucciones de despliegue
## Clonación de Repositorios

Se deben clonar los siguientes repositorios:
git clone https://gitlab.com/sib-colombia/portal-de-datos-frontend.git

## Compilación y ejecución entorno de desarrollo
En la carpeta clonada se debe realizar la instalación de dependencias y construcción, se recomienda empezar con el dataportal_v2_API:

Posteriormente se realiza la instalación del Portal de Datos:
```bash
cd portal-de-datos-frontend
npm install
```

Para ejecutar la versión de desarrollo del frontend y el backend se ejecuta en terminales independientes:
```bash
:~/portal-de-datos-frontend$ npm start
```

Para visualizar la plataforma se visita: 

http://localhost:3000

# Configuración

Se puede configurar los endpoints de los backends en el archivo:

https://gitlab.com/sib-colombia/portal-de-datos-frontend/blob/develop/src/config/const.js

